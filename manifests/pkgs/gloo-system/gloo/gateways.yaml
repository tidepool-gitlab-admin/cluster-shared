---
apiVersion: gateway.solo.io/v1
kind: Gateway
metadata:
  annotations:
    origin: default
  name: gateway-proxy
  namespace: gloo-system
spec:
  bindAddress: '::'
  bindPort: 8080
  httpGateway:
    options:
      healthCheck:
        path: /status
      httpConnectionManagerSettings:
        tracing:
          requestHeadersForTags:
          - path
          - origin
          verbose: true
        useRemoteAddress: true
    virtualServiceNamespaces:
    - '*'
    virtualServiceSelector:
      protocol: http
      type: external
  options:
    accessLoggingService:
      accessLog:
      - fileSink:
          jsonFormat:
            authority: '%REQ(:authority)%'
            authorization: '%REQ(authorization)%'
            bytesReceived: '%BYTES_RECEIVED%'
            clientName: '%REQ(x-tidepool-client-name)%'
            content: '%REQ(content-type)%'
            duration: '%DURATION%'
            forwardedFor: '%REQ(X-FORWARDED-FOR)%'
            method: '%REQ(:method)%'
            path: '%REQ(:path)%'
            referer: '%REQ(referer)%'
            remoteAddress: '%DOWNSTREAM_REMOTE_ADDRESS_WITHOUT_PORT%'
            request: '%REQ(x-tidepool-trace-request)%'
            requestDuration: '%REQUEST_DURATION%'
            response: '%RESPONSE_CODE%'
            responseCodeDetail: '%RESPONSE_CODE_DETAILS%'
            responseFlags: '%RESPONSE_FLAGS%'
            scheme: '%REQ(:scheme)%'
            session: '%REQ(x-tidepool-trace-session)%'
            startTime: '%START_TIME%'
            token: '%REQ(x-tidepool-session-token)%'
            upstream: '%UPSTREAM_CLUSTER%'
            userAgent: '%REQ(user-agent)%'
          path: /dev/stdout
  proxyNames:
  - gateway-proxy
  ssl: false
  useProxyProto: true
---
apiVersion: gateway.solo.io/v1
kind: Gateway
metadata:
  annotations:
    origin: default
  name: gateway-proxy-ssl
  namespace: gloo-system
spec:
  bindAddress: '::'
  bindPort: 8443
  httpGateway:
    options:
      healthCheck:
        path: /status
      httpConnectionManagerSettings:
        tracing:
          requestHeadersForTags:
          - path
          - origin
          verbose: true
        useRemoteAddress: true
    virtualServiceNamespaces:
    - '*'
    virtualServiceSelector:
      protocol: https
      type: external
  options:
    accessLoggingService:
      accessLog:
      - fileSink:
          jsonFormat:
            authority: '%REQ(:authority)%'
            authorization: '%REQ(authorization)%'
            bytesReceived: '%BYTES_RECEIVED%'
            clientName: '%REQ(x-tidepool-client-name)%'
            content: '%REQ(content-type)%'
            duration: '%DURATION%'
            forwardedFor: '%REQ(X-FORWARDED-FOR)%'
            method: '%REQ(:method)%'
            path: '%REQ(:path)%'
            referer: '%REQ(referer)%'
            remoteAddress: '%DOWNSTREAM_REMOTE_ADDRESS_WITHOUT_PORT%'
            request: '%REQ(x-tidepool-trace-request)%'
            requestDuration: '%REQUEST_DURATION%'
            response: '%RESPONSE_CODE%'
            responseCodeDetail: '%RESPONSE_CODE_DETAILS%'
            responseFlags: '%RESPONSE_FLAGS%'
            scheme: '%REQ(:scheme)%'
            session: '%REQ(x-tidepool-trace-session)%'
            startTime: '%START_TIME%'
            token: '%REQ(x-tidepool-session-token)%'
            upstream: '%UPSTREAM_CLUSTER%'
            userAgent: '%REQ(user-agent)%'
          path: /dev/stdout
  proxyNames:
  - gateway-proxy
  ssl: true
  useProxyProto: true
---
apiVersion: gateway.solo.io/v1
kind: Gateway
metadata:
  annotations:
    origin: default
  name: internal-gateway-proxy
  namespace: gloo-system
spec:
  bindAddress: '::'
  bindPort: 8080
  httpGateway:
    options:
      httpConnectionManagerSettings:
        tracing:
          requestHeadersForTags:
          - path
          - origin
          verbose: true
        useRemoteAddress: true
    virtualServiceNamespaces:
    - '*'
    virtualServiceSelector:
      protocol: http
      type: internal
  options:
    accessLoggingService:
      accessLog:
      - fileSink:
          jsonFormat:
            authority: '%REQ(:authority)%'
            authorization: '%REQ(authorization)%'
            bytesReceived: '%BYTES_RECEIVED%'
            clientName: '%REQ(x-tidepool-client-name)%'
            content: '%REQ(content-type)%'
            duration: '%DURATION%'
            forwardedFor: '%REQ(X-FORWARDED-FOR)%'
            method: '%REQ(:method)%'
            path: '%REQ(:path)%'
            referer: '%REQ(referer)%'
            remoteAddress: '%DOWNSTREAM_REMOTE_ADDRESS_WITHOUT_PORT%'
            request: '%REQ(x-tidepool-trace-request)%'
            requestDuration: '%REQUEST_DURATION%'
            response: '%RESPONSE_CODE%'
            responseCodeDetail: '%RESPONSE_CODE_DETAILS%'
            responseFlags: '%RESPONSE_FLAGS%'
            scheme: '%REQ(:scheme)%'
            session: '%REQ(x-tidepool-trace-session)%'
            startTime: '%START_TIME%'
            token: '%REQ(x-tidepool-session-token)%'
            upstream: '%UPSTREAM_CLUSTER%'
            userAgent: '%REQ(user-agent)%'
          path: /dev/stdout
  proxyNames:
  - internal-gateway-proxy
  ssl: false
  useProxyProto: false
---
apiVersion: gateway.solo.io/v1
kind: Gateway
metadata:
  annotations:
    origin: default
  name: internal-gateway-proxy-ssl
  namespace: gloo-system
spec:
  bindAddress: '::'
  bindPort: 8443
  httpGateway:
    options:
      httpConnectionManagerSettings:
        tracing:
          requestHeadersForTags:
          - path
          - origin
          verbose: true
        useRemoteAddress: true
    virtualServiceNamespaces:
    - '*'
    virtualServiceSelector:
      protocol: https
      type: internal
  options:
    accessLoggingService:
      accessLog:
      - fileSink:
          jsonFormat:
            authority: '%REQ(:authority)%'
            authorization: '%REQ(authorization)%'
            bytesReceived: '%BYTES_RECEIVED%'
            clientName: '%REQ(x-tidepool-client-name)%'
            content: '%REQ(content-type)%'
            duration: '%DURATION%'
            forwardedFor: '%REQ(X-FORWARDED-FOR)%'
            method: '%REQ(:method)%'
            path: '%REQ(:path)%'
            referer: '%REQ(referer)%'
            remoteAddress: '%DOWNSTREAM_REMOTE_ADDRESS_WITHOUT_PORT%'
            request: '%REQ(x-tidepool-trace-request)%'
            requestDuration: '%REQUEST_DURATION%'
            response: '%RESPONSE_CODE%'
            responseCodeDetail: '%RESPONSE_CODE_DETAILS%'
            responseFlags: '%RESPONSE_FLAGS%'
            scheme: '%REQ(:scheme)%'
            session: '%REQ(x-tidepool-trace-session)%'
            startTime: '%START_TIME%'
            token: '%REQ(x-tidepool-session-token)%'
            upstream: '%UPSTREAM_CLUSTER%'
            userAgent: '%REQ(user-agent)%'
          path: /dev/stdout
  proxyNames:
  - internal-gateway-proxy
  ssl: true
  useProxyProto: false
---
apiVersion: gateway.solo.io/v1
kind: Gateway
metadata:
  annotations:
    origin: default
  name: pomerium-proxy
  namespace: gloo-system
spec:
  bindAddress: '::'
  bindPort: 8080
  httpGateway:
    options:
      healthCheck:
        path: /status
      httpConnectionManagerSettings:
        tracing:
          requestHeadersForTags:
          - path
          - origin
          verbose: true
        useRemoteAddress: true
    virtualServiceNamespaces:
    - '*'
    virtualServiceSelector:
      protocol: http
      type: pomerium
  options:
    accessLoggingService:
      accessLog:
      - fileSink:
          jsonFormat:
            authority: '%REQ(:authority)%'
            authorization: '%REQ(authorization)%'
            bytesReceived: '%BYTES_RECEIVED%'
            clientName: '%REQ(x-tidepool-client-name)%'
            content: '%REQ(content-type)%'
            duration: '%DURATION%'
            forwardedFor: '%REQ(X-FORWARDED-FOR)%'
            method: '%REQ(:method)%'
            path: '%REQ(:path)%'
            referer: '%REQ(referer)%'
            remoteAddress: '%DOWNSTREAM_REMOTE_ADDRESS_WITHOUT_PORT%'
            request: '%REQ(x-tidepool-trace-request)%'
            requestDuration: '%REQUEST_DURATION%'
            response: '%RESPONSE_CODE%'
            responseCodeDetail: '%RESPONSE_CODE_DETAILS%'
            responseFlags: '%RESPONSE_FLAGS%'
            scheme: '%REQ(:scheme)%'
            session: '%REQ(x-tidepool-trace-session)%'
            startTime: '%START_TIME%'
            token: '%REQ(x-tidepool-session-token)%'
            upstream: '%UPSTREAM_CLUSTER%'
            userAgent: '%REQ(user-agent)%'
          path: /dev/stdout
  proxyNames:
  - pomerium-gateway-proxy
  ssl: false
  useProxyProto: true
---
apiVersion: gateway.solo.io/v1
kind: Gateway
metadata:
  annotations:
    origin: default
  name: pomerium-proxy-ssl
  namespace: gloo-system
spec:
  bindAddress: '::'
  bindPort: 8443
  httpGateway:
    options:
      healthCheck:
        path: /status
      httpConnectionManagerSettings:
        tracing:
          requestHeadersForTags:
          - path
          - origin
          verbose: true
        useRemoteAddress: true
    virtualServiceNamespaces:
    - '*'
    virtualServiceSelector:
      protocol: https
      type: pomerium
  options:
    accessLoggingService:
      accessLog:
      - fileSink:
          jsonFormat:
            authority: '%REQ(:authority)%'
            authorization: '%REQ(authorization)%'
            bytesReceived: '%BYTES_RECEIVED%'
            clientName: '%REQ(x-tidepool-client-name)%'
            content: '%REQ(content-type)%'
            duration: '%DURATION%'
            forwardedFor: '%REQ(X-FORWARDED-FOR)%'
            method: '%REQ(:method)%'
            path: '%REQ(:path)%'
            referer: '%REQ(referer)%'
            remoteAddress: '%DOWNSTREAM_REMOTE_ADDRESS_WITHOUT_PORT%'
            request: '%REQ(x-tidepool-trace-request)%'
            requestDuration: '%REQUEST_DURATION%'
            response: '%RESPONSE_CODE%'
            responseCodeDetail: '%RESPONSE_CODE_DETAILS%'
            responseFlags: '%RESPONSE_FLAGS%'
            scheme: '%REQ(:scheme)%'
            session: '%REQ(x-tidepool-trace-session)%'
            startTime: '%START_TIME%'
            token: '%REQ(x-tidepool-session-token)%'
            upstream: '%UPSTREAM_CLUSTER%'
            userAgent: '%REQ(user-agent)%'
          path: /dev/stdout
  proxyNames:
  - pomerium-gateway-proxy
  ssl: true
  useProxyProto: true
